FROM centos AS builder
RUN yum update -y &&  rpm -Uvh https://packages.microsoft.com/config/centos/8/packages-microsoft-prod.rpm && \
    yum  -y install  dotnet-sdk-5.0  && mkdir -p /app 
COPY /app  /app
WORKDIR /app
RUN dotnet dev-certs https
FROM centos
RUN yum install dotnet-runtime-3.1 -y && mkdir -p /app 
COPY --from=builder /app  /app 
WORKDIR /app
ENTRYPOINT ["dotnet", "test.dll" ] 